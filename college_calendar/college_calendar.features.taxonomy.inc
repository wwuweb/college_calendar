<?php
/**
 * @file
 * college_calendar.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function college_calendar_taxonomy_default_vocabularies() {
  return array(
    'calendars' => array(
      'name' => 'Calendars',
      'machine_name' => 'calendars',
      'description' => 'Allow users to put events on certain calendars.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
