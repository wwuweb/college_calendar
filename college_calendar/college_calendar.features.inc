<?php
/**
 * @file
 * college_calendar.features.inc
 */

/**
 * Implements hook_views_api().
 */
function college_calendar_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function college_calendar_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => t('Use the <em>Event</em> content type to post events to the website calendar.'),
      'has_title' => '1',
      'title_label' => t('Event Title'),
      'help' => '',
    ),
  );
  return $items;
}
